<?php
/**
 * @file
 * gk_webform.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_webform_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:webform:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'webform';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '4d3c07b3-6c25-490c-af68-de7c5970bb58';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a2ee1451-7768-4ee2-b664-407e0c136e7d';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a2ee1451-7768-4ee2-b664-407e0c136e7d';
    $display->content['new-a2ee1451-7768-4ee2-b664-407e0c136e7d'] = $pane;
    $display->panels['primary'][0] = 'new-a2ee1451-7768-4ee2-b664-407e0c136e7d';
    $pane = new stdClass();
    $pane->pid = 'new-cb8ed65a-b3f7-4028-bfc1-4067a9cb6267';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'cb8ed65a-b3f7-4028-bfc1-4067a9cb6267';
    $display->content['new-cb8ed65a-b3f7-4028-bfc1-4067a9cb6267'] = $pane;
    $display->panels['primary'][1] = 'new-cb8ed65a-b3f7-4028-bfc1-4067a9cb6267';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:webform:default'] = $panelizer;

  return $export;
}
